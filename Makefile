install-deps:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.29
	go install -mod=mod google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.3

get-deps:
	go get -u google.golang.org/protobuf/cmd/protoc-gen-go
	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc

generate:
	mkdir -p pkg/time/v1 > /dev/null 2>&1
	protoc --proto_path api/grpc/time/v1 \
			--proto_path api/grpc/protovalidate \
			--go_out=pkg/time/v1 --go_opt=paths=source_relative \
			--plugin=protoc-gen-go=${GOBIN}/protoc-gen-go \
			--go-grpc_out=pkg/time/v1 --go-grpc_opt=paths=source_relative \
			--plugin=protoc-gen-go-grpc=${GOBIN}/protoc-gen-go-grpc \
			time.proto
